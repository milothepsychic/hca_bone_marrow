# This file serves the r_*() functions (e.g. r_make()) documented at
# https://ropenscilabs.github.io/drake-manual/projects.html#safer-interactivity # nolint
# and
# https://ropensci.github.io/drake/reference/r_make.html

# Load your packages and supporting functions into your session.
# If you use supporting scripts like the ones below,
# you will need to supply them yourself. Examples:
# https://github.com/wlandau/drake-examples/tree/master/main/R
reticulate::use_condaenv(condaenv = "rtools", required = TRUE)
options(future.globals.maxSize = +Inf)

source("R/packages.R")  # Load your packages, e.g. library(drake).
source("R/functions.R") # Define your custom code as a bunch of functions.

### Setup project variables
subset_data = NULL
multisample = FALSE

### Setup file locations
count_file = "/home/milo/datasets/hca/ica_bone_marrow_h5.h5"
metadata_file = "/home/milo/datasets/hca/cc95ff89-2e68-4a08-a234-480eca21ce79.tsv"

### Run Parameters
batch_variables = c("sample","donor_ethnicity")

source("R/plan.R")
# config <- drake_config(plan)
# vis_drake_graph(config)

### Setup multiprocessing
BPPARAM = SnowParam(workers=parallel::detectCores(), type = "SOCK")
BiocParallel::register(BPPARAM)
future::plan(strategy = "multiprocess")

# _drake.R must end with a call to drake_config().
# The arguments to drake_config() are basically the same as those to make().
scrna_plan <- drake_config(plan,
             verbose = 2,
             parallelism = "future",
             jobs = 1,
             lock_envir = TRUE)
