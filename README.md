[![pipeline status](https://gitlab.com/milothepsychic/rnaseq_drake/badges/master/pipeline.svg)](https://gitlab.com/milothepsychic/rnaseq_drake/commits/master)

Drake pipeline to process RNAseq data in R using DESeq, including differential
gene expression analysis and module score assignment.

### Singularity
Use the following command:
```
srun --mem=128 --cpus-per-task=12 --partition=serial singularity run -B /s/guth-aci:/scratch ../rnaseq_drake_rocker.simg Rscript -e "drake::r_make\(\)"
```